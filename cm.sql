USE kb301_dolgorukov;
GO

IF SCHEMA_ID('lab_3') IS NOT NULL
    DROP TABLE IF EXISTS lab_3.movements;
    DROP TABLE IF EXISTS lab_3.plates;
    DROP TABLE IF EXISTS lab_3.posts;
    DROP TABLE IF EXISTS lab_3.regions_plate;
    DROP TABLE IF EXISTS lab_3.regions;
    DROP TABLE IF EXISTS lab_3.directions;
    DROP FUNCTION IF EXISTS lab_3.find_id_plate;
    DROP PROCEDURE IF EXISTS lab_3.insert_plate;
    DROP PROCEDURE IF EXISTS lab_3.insert_movement;
    DROP VIEW IF EXISTS lab_3.movements_resident;
    DROP VIEW IF EXISTS lab_3.movements_transit;
    DROP VIEW IF EXISTS lab_3.movements_nonresident;
    DROP FUNCTION IF EXISTS lab_3.report_movements_resident;
    DROP FUNCTION IF EXISTS lab_3.report_movements_transit;
    DROP FUNCTION IF EXISTS lab_3.report_movements_nonresident;
    DROP SCHEMA lab_3;
GO

CREATE SCHEMA lab_3;
GO

-- Посты ГАИ.
CREATE TABLE lab_3.posts(
    id TINYINT PRIMARY KEY IDENTITY(0, 1),
    title NVARCHAR(32)
);

INSERT lab_3.posts VALUES
(N'Северный')
, (N'Западный')
, (N'Южный')
, (N'Восточный');
GO

-- Регионы.
CREATE TABLE lab_3.regions(
    id TINYINT PRIMARY KEY,
    title NVARCHAR(64)
);

INSERT lab_3.regions VALUES
(66, N'Свердловская область')
, (59, N'Пермский край')
, (2, N'Республика Башкортостан')
, (74, N'Челябинская область')
, (45, N'Курганская область')
, (72, N'Тюменская область')
, (86, N'Ханты-Мансийский автономный округ - Югра');
GO

-- Сооответствие кодов регионов с отображающихся на автомобильных номерах кодов
-- регионов.
CREATE TABLE lab_3.regions_plate(
    id_region TINYINT FOREIGN KEY REFERENCES lab_3.regions(id),
    id_region_plate VARCHAR(3) PRIMARY KEY
);

INSERT lab_3.regions_plate VALUES

-- Свердловская область
(66, '66')
, (66, '96')
, (66, '196')

-- Пермский край
, (59, '59')
, (59, '81')
, (59, '159')

-- Республика Башкортостан
, (2, '02')
, (2, '102')
, (2, '702')

-- Челябинская область
, (74, '74')
, (74, '174')
, (74, '774')

-- Курганская область
, (45, '45')

-- Тюменская область
, (72, '72')
, (72, '172')

-- Ханты-Мансийский автономный округ - Югра
, (86, '86')
, (86, '186');
GO

-- Направления
CREATE TABLE lab_3.directions(
    id BIT PRIMARY KEY,
    title NVARCHAR(8)
);

INSERT lab_3.directions VALUES
(0, N'Выезд'),
(1, N'Въезд')
GO

-- Таблица автомобильных номеров.
CREATE TABLE lab_3.plates(
    id INT PRIMARY KEY IDENTITY(0, 1),

    -- Серия.
    batch CHAR(3),

    -- Регистрационный номер.
    number_reg CHAR(3),

    -- Код региона регистрации.
    region_reg VARCHAR(3) FOREIGN KEY REFERENCES lab_3.regions_plate(id_region_plate),
);

-- Ограничение на серию.
ALTER TABLE lab_3.plates
ADD CHECK (batch LIKE '[ABCEHKMOPXTY][ABCEHKMOPXTY][ABCEHKMOPXTY]');

-- Ограничение на регистрационный номер.
ALTER TABLE lab_3.plates
ADD CHECK (number_reg LIKE '[0-9][0-9][0-9]' AND number_reg != '000')
GO

-- Триггер, повышает регистр серии номера. Срабатывает после занесения номера в
-- базу.
CREATE OR ALTER TRIGGER lab_3.up_case_on_insert_plate
ON lab_3.plates AFTER INSERT
AS
BEGIN
    UPDATE lab_3.plates SET
    batch = upper(batch)
END;
GO

-- Триггер, поддерживающий только уникальные значения в таблице автомобильных
-- номеров.
CREATE OR ALTER TRIGGER lab_3.only_unique_on_insert_plate
ON lab_3.plates INSTEAD OF INSERT
AS
BEGIN
    DECLARE for_all_inserted CURSOR LOCAL READ_ONLY FOR
    SELECT * FROM inserted;

    OPEN for_all_inserted;

    DECLARE @id INT = -1;
    DECLARE @batch CHAR(3);
    DECLARE @number_reg CHAR(3);
    DECLARE @region_reg VARCHAR(3);

    FETCH NEXT FROM for_all_inserted
    INTO @id, @batch, @number_reg, @region_reg;

    WHILE @@FETCH_STATUS = 0
    BEGIN
        DECLARE @id_found INT = -1;

        SELECT TOP 1 @id_found = id FROM lab_3.plates
        WHERE @batch = batch AND
              @number_reg = number_reg AND
              @region_reg = region_reg;

        IF @id_found = -1
            INSERT lab_3.plates VALUES
            (@batch, @number_reg, @region_reg)

        FETCH NEXT FROM for_all_inserted
        INTO @id, @batch, @number_reg, @region_reg;
    END

    CLOSE for_all_inserted;
END;
GO

-- Поиск идентификатора по автомобильному номеру.
CREATE OR ALTER FUNCTION lab_3.find_id_plate(
    @plate VARCHAR(9)
) RETURNS INT
AS
BEGIN
    DECLARE @batch CHAR(3);
    DECLARE @number_reg CHAR(3);
    DECLARE @region_reg VARCHAR(3);
    DECLARE @id_found INT;

    SELECT @batch = substring(@plate, 1, 1) + substring(@plate, 5, 2);
    SELECT @batch = upper(@batch);
    SELECT @number_reg = substring(@plate, 2, 3);
    SELECT @region_reg = substring(@plate, 7, 3);

    SELECT @id_found = -1;

    SELECT TOP 1 @id_found = id FROM lab_3.plates
    WHERE batch = @batch AND
          number_reg = @number_reg AND
          region_reg = @region_reg;

    RETURN @id_found;
END;
GO

-- Таблица «Передвижения».
CREATE TABLE lab_3.movements(
    id INT PRIMARY KEY IDENTITY(0, 1),
    id_post TINYINT FOREIGN KEY REFERENCES lab_3.posts(id),
    id_plate INT FOREIGN KEY REFERENCES lab_3.plates(id),
    stamp TIME,
    direction BIT FOREIGN KEY REFERENCES lab_3.directions(id)
);
GO

-- Триггер, вызывающийся при занесении информацию в отношение «Передвижения».
CREATE OR ALTER TRIGGER lab_3.on_fixation
ON lab_3.movements INSTEAD OF INSERT
AS
BEGIN
    DECLARE for_all_inserted CURSOR LOCAL READ_ONLY FOR
    SELECT * FROM inserted ORDER BY stamp ASC;

    OPEN for_all_inserted;

    DECLARE @id INT = NULL;
    DECLARE @id_post TINYINT = NULL;
    DECLARE @id_plate INT = NULL;
    DECLARE @stamp TIME = NULL;
    DECLARE @direction BIT = NULL;

    FETCH NEXT FROM for_all_inserted
    INTO @id, @id_post, @id_plate, @stamp, @direction;

    WHILE @@FETCH_STATUS = 0
    BEGIN
        DECLARE @id_last INT = -1;
        DECLARE @id_post_last TINYINT = NULL;
        DECLARE @stamp_last TIME = NULL;
        DECLARE @direction_last BIT = NULL;

        SELECT TOP 1 @id_last = id,
            @id_post_last = id_post,
            @stamp_last = stamp,
            @direction_last = direction FROM lab_3.movements
        WHERE @id_plate = lab_3.movements.id_plate
        ORDER BY stamp DESC;

        IF @id_last != -1
            DECLARE @diff INT = DATEDIFF(MINUTE, @stamp_last, @stamp);

            IF @diff < 0
                THROW 50001, N'Есть перемещение позднее.', 1

            IF @diff < 5
                THROW 50001, N'Временной промежуток слишком мал.', 2

            IF @direction_last = @direction
                THROW 50001, N'Совпадение направлений.', 3

        INSERT lab_3.movements VALUES
        (@id_post, @id_plate, @stamp, @direction)

        FETCH NEXT FROM for_all_inserted
        INTO @id, @id_post, @id_plate, @stamp, @direction;

    END

    CLOSE for_all_inserted;
END;
GO

-- Добавить автомобильный номер в базу.
CREATE OR ALTER PROCEDURE lab_3.insert_plate(
    @plate VARCHAR(9)
)
AS
BEGIN
    DECLARE @batch CHAR(3);
    DECLARE @number_reg CHAR(3);
    DECLARE @region_reg VARCHAR(3);

    SELECT @batch = substring(@plate, 1, 1) + substring(@plate, 5, 2);
    SELECT @number_reg = substring(@plate, 2, 3);
    SELECT @region_reg = substring(@plate, 7, 3);

    SELECT @batch = upper(@batch);

    INSERT lab_3.plates VALUES
    (@batch, @number_reg, @region_reg)
END;
GO

-- Зафиксировать передвижение.
-- @plate — автомобильный номер,
-- @post — имя поста,
-- @direction — направление,
-- @stamp — время.
CREATE OR ALTER PROCEDURE lab_3.insert_movement(
    @plate VARCHAR(9),
    @post NVARCHAR(32),
    @direction NVARCHAR(8),
    @stamp TIME
)
AS
BEGIN
    DECLARE @id_plate TINYINT = NULL;
    DECLARE @id_post TINYINT = NULL;
    DECLARE @id_direction INT = NULL;

    EXEC lab_3.insert_plate @plate=@plate;
    EXEC @id_plate = lab_3.find_id_plate @plate=@plate;

    IF @id_plate IS NULL
        THROW 50002, N'Номер не соответствует формату.', 1

    SELECT TOP 1 @id_post = id FROM lab_3.posts
    WHERE @post LIKE title;

    IF @id_post IS NULL
        THROW 50002, N'Поста нет.', 2

    SELECT TOP 1 @id_direction = id FROM lab_3.directions
    WHERE @direction LIKE title;

    IF @id_direction IS NULL
        THROW 50003, N'Направления нет.', 3

    INSERT lab_3.movements VALUES
    (@id_post, @id_plate, @stamp, @id_direction)
END;
GO

-- Представление «Перемещения транзитных».
CREATE OR ALTER VIEW lab_3.movements_transit
AS
    SELECT C.* FROM lab_3.movements C
    JOIN (SELECT A.id_plate
    , A.stamp AS N'Первый въезд'
    , B.stamp AS N'Последний выезд'
    FROM lab_3.movements A, lab_3.movements B
    WHERE A.id_plate = B.id_plate AND
        A.direction = 1 AND
        B.direction = 0 AND
        datediff(MINUTE, a.stamp, b.stamp) > 0 AND

        A.stamp = (
            SELECT min(E.stamp) FROM lab_3.movements E
            WHERE E.id_plate = A.id_plate
        ) AND

        B.stamp = (
            SELECT max(E.stamp) FROM lab_3.movements E
            WHERE E.id_plate = A.id_plate
        ) AND

        A.id_post != B.id_post

    GROUP BY A.id_plate, A.stamp, B.stamp) AS D
    ON C.id_plate = D.id_plate

    JOIN (select id_plate, count(*) as amount from lab_3.movements group by id_plate) F
    ON C.id_plate = F.id_plate AND F.amount % 2 = 0;
GO

-- Представление «Перемещение местных».
CREATE OR ALTER VIEW lab_3.movements_resident
AS
    SELECT C.* FROM lab_3.movements C
    JOIN (SELECT A.id_plate
    , A.stamp AS N'Первый выезд'
    , B.stamp AS N'Последний въезд'
    FROM lab_3.movements A, lab_3.movements B
    WHERE A.id_plate = B.id_plate AND
          A.direction = 0 AND
          B.direction = 1 AND
          datediff(MINUTE, a.stamp, b.stamp) > 0 AND

          B.stamp = (
            SELECT max(E.stamp) FROM lab_3.movements E
            WHERE E.id_plate = A.id_plate
          ) AND

          A.stamp = (
            SELECT min(E.stamp) FROM lab_3.movements E
            WHERE E.id_plate = A.id_plate
          )

    GROUP BY A.id_plate, A.stamp, B.stamp) AS D
    ON C.id_plate = D.id_plate

    JOIN (select id_plate, count(*) as amount from lab_3.movements group by id_plate) F
    ON C.id_plate = F.id_plate AND F.amount % 2 = 0 AND
    C.id_plate NOT IN (SELECT id_plate FROM lab_3.movements_transit)
GO

-- Представление «Перемещения иногородних».
CREATE OR ALTER VIEW lab_3.movements_nonresident
AS
    SELECT C.* FROM lab_3.movements C
    JOIN (SELECT A.id_plate
    , A.stamp AS N'Первый въезд'
    , B.stamp AS N'Последний выезд'
    FROM lab_3.movements A, lab_3.movements B
    WHERE A.id_plate = B.id_plate AND
        A.direction = 1 AND
        B.direction = 0 AND
        datediff(MINUTE, a.stamp, b.stamp) > 0 AND

        A.stamp = (
            SELECT min(E.stamp) FROM lab_3.movements E
            WHERE E.id_plate = A.id_plate
        ) AND

        B.stamp = (
            SELECT max(E.stamp) FROM lab_3.movements E
            WHERE E.id_plate = A.id_plate
        ) AND

        A.id_post = B.id_post

    GROUP BY A.id_plate, A.stamp, B.stamp) AS D
    ON C.id_plate = D.id_plate

    JOIN (select id_plate, count(*) as amount from lab_3.movements group by id_plate) F
    ON C.id_plate = F.id_plate AND
       F.amount % 2 = 0 AND
       C.id_plate NOT IN (SELECT id_plate FROM lab_3.movements_resident)
GO

-- Функция составления отчёта по передвижениям местных.
CREATE OR ALTER FUNCTION lab_3.report_movements_resident()
RETURNS TABLE
AS
    RETURN SELECT
    substring(batch, 1, 1) + number_reg + substring(batch, 2, 2) + region_reg AS N'Номер'
    , lab_3.regions.title as N'Регион'
    , lab_3.posts.title AS N'Пост'
    , stamp AS N'Время'
    , lab_3.directions.title AS N'Направление'
    FROM lab_3.movements_resident
    JOIN lab_3.posts ON id_post = lab_3.posts.id
    JOIN lab_3.plates ON id_plate = lab_3.plates.id
    JOIN lab_3.directions ON direction = lab_3.directions.id
    JOIN lab_3.regions_plate ON region_reg = id_region_plate
    JOIN lab_3.regions ON id_region = lab_3.regions.id;
GO

-- Функция составления отчёта по передвижениям транзитов.
CREATE OR ALTER FUNCTION lab_3.report_movements_transit()
RETURNS TABLE
AS
    RETURN SELECT
    substring(batch, 1, 1) + number_reg + substring(batch, 2, 2) + region_reg AS N'Номер'
    , lab_3.regions.title as N'Регион'
    , lab_3.posts.title AS N'Пост'
    , stamp AS N'Время'
    , lab_3.directions.title AS N'Направление'
    FROM lab_3.movements_transit
    JOIN lab_3.posts ON id_post = lab_3.posts.id
    JOIN lab_3.plates ON id_plate = lab_3.plates.id
    JOIN lab_3.directions ON direction = lab_3.directions.id
    JOIN lab_3.regions_plate ON region_reg = id_region_plate
    JOIN lab_3.regions ON id_region = lab_3.regions.id;
GO

-- Функция составления отчёта по передвижениям иногородних.
CREATE OR ALTER FUNCTION lab_3.report_movements_nonresident()
RETURNS TABLE
AS
    RETURN SELECT
    substring(batch, 1, 1) + number_reg + substring(batch, 2, 2) + region_reg AS N'Номер'
    , lab_3.regions.title as N'Регион'
    , lab_3.posts.title AS N'Пост'
    , stamp AS N'Время'
    , lab_3.directions.title AS N'Направление'
    FROM lab_3.movements_nonresident
    JOIN lab_3.posts ON id_post = lab_3.posts.id
    JOIN lab_3.plates ON id_plate = lab_3.plates.id
    JOIN lab_3.directions ON direction = lab_3.directions.id
    JOIN lab_3.regions_plate ON region_reg = id_region_plate
    JOIN lab_3.regions ON id_region = lab_3.regions.id;
GO
